# Contribution Guide

Thank you for your interest in contributing to Linstrom! All contributors are welcome, regardless of their level of experience.

## Bug Reports

Use the [bug report issue template](https://gitlab.com/mstarongitlab/linstrom/issues/new?template=bug-report.md) to file a bug report. Please include a detailed description of the events leading up to the problem, your system configuration, and the program logs. If you're able to reproduce the bug reliably, attaching a debugger to the program, triggering it, and uploading the results would be very helpful.

This section *should* tell you how to find your logs, attach the debugger, and do whatever else you need for a detailed bug report. But nobody filled it out. Attach a picture of Goatse to your bug reports until we fix this.

## Feature Requests

Use the [feature request issue template](https://gitlab.com/mstarongitlab/linstrom/issues/new?template=suggestion.md) to suggest new features. Please note that we haven't replaced this placeholder text with the actual criteria we're looking for, which means you should spam us with utterly nonsensical ideas.

## Submitting Translations

Translation files are part of the project codebase, so you'll have to fork the repository and file a pull request (see [Contributing Code](CONTRIBUTING.md#contributing-code) below). You don't need any programming knowledge to edit the translation files, though.

This should have been removed and replaced with a quick overview of where the files are and what translators need to do in order to edit them. Nobody did that, so think of this as a free pass to scream profanities into the issue tracker in your native language.

## Contributing Code

### Forking

If you'd like to have a go at writing some code for Linstrom, fork the repository, then create a new branch with a name that describes the changes you're making. If there's a [relevant issue](https://gitlab.com/mstarongitlab/linstrom/issues), include the issue number in the branch name:

```sh
git checkout -b 1337-prevent-computer-from-exploding
```

### Development Environment

The project utilises Go (version 1.23+) for almost everything and node/npm (version 18+) for building the frontend.
The go side also makes use of `go generate` for multiple things, primarely in the storage module

We don't have a development environment, because nobody bothered to fill this out. Please add a new build system to the project specifically for your modifications. Bonus points if it's entirely nonsensical, like `npm` in a C project.

### Code Style

For go: gofmt rules and should be followed closely

For anything node: uhhh, yolo, idk yet

### Pull Requests

Once your modifications are complete, you'll want to fetch the latest changes from this repository, rebase your branch, and publish your changes:

```sh
git remote add upstream https://gitlab.com/mstarongitlab/linstrom.git
git checkout main
git pull upstream main
git checkout 1337-prevent-computer-from-exploding
git rebase master
git push --set-upstream origin 1337-prevent-computer-from-exploding
```

Finally, you can [create a pull request](https://gitlab.com/mstarongitlab/linstrom/pulls). It might not get approved, or you might have to make some additional changes to your code - but don't give up!
