package main

import "flag"

var (
	flagPrettyPrint *bool = flag.Bool(
		"pretty",
		false,
		"If set, pretty prints logging entries which would be json objects otherwise",
	)
	flagConfigFile *string = flag.String(
		"config",
		"config.toml",
		"Location of the config file. Defaults to \"config.toml\"",
	)
	flagLogLevel *string = flag.String(
		"loglevel",
		"Info",
		"Set the logging level. Options are: Trace, Debug, Info, Warning, Error, Fatal. Capitalisation doesn't matter. Defaults to Info",
	)
	flagConfigOnly *bool = flag.Bool(
		"validate-config",
		false,
		"If set, the server will only validate the config (or write the default one) and then quit",
	)
)

func init() {
	flag.Parse()
}
