package server

const ContextKeyPasskeyUsername = "context-passkey-username"

type ContextKey string

const (
	ContextKeyStorage ContextKey = "Context key for storage"
	ContextKeyActorId ContextKey = "Context key for actor id"
)

const (
	HttpErrIdPlaceholder = iota
	HttpErrIdMissingContextValue
	HttpErrIdDbFailure
	HttpErrIdNotAuthenticated
	HttpErrIdJsonMarshalFail
	HttpErrIdBadRequest
	HttpErrIdAlreadyExists
	HttpErrIdNotFound
	HttpErrIdConversionFailure
	HttpErrIdNotAllowed
)
