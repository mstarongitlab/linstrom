package server

import "net/http"

// Mounted at /api
func setupApiRouter() http.Handler {
	router := http.NewServeMux()
	router.Handle("/linstrom/", http.StripPrefix("/linstrom", setupLinstromApiRouter()))

	// Section MastoApi
	// First segment are endpoints that will need to be moved to primary router since at top route
	router.HandleFunc("GET /oauth/authorize", placeholderEndpoint)
	router.HandleFunc("POST /oauth/token", placeholderEndpoint)
	router.HandleFunc("POST /oauth/revoke", placeholderEndpoint)
	router.HandleFunc("GET /.well-known/oauth-authorization-server", placeholderEndpoint)
	// These ones are actually mounted under /api/
	router.HandleFunc("POST /v1/apps", placeholderEndpoint)
	router.HandleFunc("GET /v1/apps/verify_credentials", placeholderEndpoint)
	router.HandleFunc("POST /v1/emails/confirmations", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/verify_credentials", placeholderEndpoint)
	router.HandleFunc("PATCH /v1/accounts/update_credentials", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/{id}/statuses", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/{id}/followers", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/{id}/following", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/{id}/featured_tags", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/{id}/lists", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/follow", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/unfollow", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/remove_from_followers", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/block", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/unblock", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/mute", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/unmute", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/pin", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/unpin", placeholderEndpoint)
	router.HandleFunc("POST /v1/accounts/{id}/note", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/relationships", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/familiar_followers", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/search", placeholderEndpoint)
	router.HandleFunc("GET /v1/accounts/lookup", placeholderEndpoint)
	router.HandleFunc("GET /v1/bookmarks", placeholderEndpoint)
	router.HandleFunc("GET /v1/favourites", placeholderEndpoint)
	router.HandleFunc("GET /v1/mutes", placeholderEndpoint)
	router.HandleFunc("GET /v1/blocks", placeholderEndpoint)
	router.HandleFunc("GET /v1/domain_blocks", placeholderEndpoint)
	router.HandleFunc("POST /v1/domain_blocks", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/domain_blocks", placeholderEndpoint)
	router.HandleFunc("GET /v2/filters", placeholderEndpoint)
	router.HandleFunc("GET /v2/filters/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v2/filters", placeholderEndpoint)
	router.HandleFunc("PUT /v2/filters/{id}", placeholderEndpoint)
	router.HandleFunc("DELETE /v2/filters/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v2/filters/:filter_id/keywords", placeholderEndpoint)
	router.HandleFunc("POST /v2/filters/:filter_id/keywords", placeholderEndpoint)
	router.HandleFunc("GET /v2/filters/keywords/{id}", placeholderEndpoint)
	router.HandleFunc("PUT /v2/filters/keywords/{id}", placeholderEndpoint)
	router.HandleFunc("DELETE /v2/filters/keywords/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v2/filters/:filter_id/statuses", placeholderEndpoint)
	router.HandleFunc("POST /v2/filters/:filter_id/statuses", placeholderEndpoint)
	router.HandleFunc("GET /v2/filters/statuses/{id}", placeholderEndpoint)
	router.HandleFunc("DELETE /v2/filters/statuses/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/reports", placeholderEndpoint)
	router.HandleFunc("GET /v1/follow_requests", placeholderEndpoint)
	router.HandleFunc("POST /v1/follow_requests/:account_id/authorize", placeholderEndpoint)
	router.HandleFunc("POST /v1/follow_requests/:account_id/reject", placeholderEndpoint)
	router.HandleFunc("GET /v1/endorsements", placeholderEndpoint)
	router.HandleFunc("GET /v1/featured_tags", placeholderEndpoint)
	router.HandleFunc("POST /v1/featured_tags", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/featured_tags/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/featured_tags/suggestions", placeholderEndpoint)
	router.HandleFunc("GET /v1/preferences", placeholderEndpoint)
	router.HandleFunc("GET /v1/followed_tags", placeholderEndpoint)
	router.HandleFunc("GET /v2/suggestions", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/suggestions/:account_id", placeholderEndpoint)
	router.HandleFunc("GET /v1/tags/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/tags/{id}/follow", placeholderEndpoint)
	router.HandleFunc("POST /v1/tags/{id}/unfollow", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/profile/avatar", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/profile/header", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses", placeholderEndpoint)
	router.HandleFunc("GET /v1/statuses/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/statuses", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/statuses/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/statuses/{id}/context", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/translate", placeholderEndpoint)
	router.HandleFunc("GET /v1/statuses/{id}/reblogged_by", placeholderEndpoint)
	router.HandleFunc("GET /v1/statuses/{id}/favourited_by", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/favourite", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/unfavourite", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/reblog", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/unreblog", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/bookmark", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/unbookmark", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/mute", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/unmute", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/pin", placeholderEndpoint)
	router.HandleFunc("POST /v1/statuses/{id}/unpin", placeholderEndpoint)
	router.HandleFunc("PUT /v1/statuses/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/statuses/{id}/history", placeholderEndpoint)
	router.HandleFunc("GET /v1/statuses/{id}/source", placeholderEndpoint)
	router.HandleFunc("POST /v2/media", placeholderEndpoint)
	router.HandleFunc("GET /v1/media/{id}", placeholderEndpoint)
	router.HandleFunc("PUT /v1/media/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/polls/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/polls/{id}/votes", placeholderEndpoint)
	router.HandleFunc("GET /v1/scheduled_statuses", placeholderEndpoint)
	router.HandleFunc("GET /v1/scheduled_statuses/{id}", placeholderEndpoint)
	router.HandleFunc("PUT /v1/scheduled_statuses/{id}", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/scheduled_statuses/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/timelines/public", placeholderEndpoint)
	router.HandleFunc("GET /v1/timelines/tag/:hashtag", placeholderEndpoint)
	router.HandleFunc("GET /v1/timelines/home", placeholderEndpoint)
	router.HandleFunc("GET /v1/timelines/link", placeholderEndpoint) // ?url=:url
	router.HandleFunc("GET /v1/timelines/list/:list_id", placeholderEndpoint)
	router.HandleFunc("GET /v1/conversations", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/conversations/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/conversations/{id}/read", placeholderEndpoint)
	router.HandleFunc("GET /v1/lists", placeholderEndpoint)
	router.HandleFunc("GET /v1/lists/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/lists", placeholderEndpoint)
	router.HandleFunc("PUT /v1/lists/{id}", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/lists/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/lists/{id}/accounts", placeholderEndpoint)
	router.HandleFunc("POST /v1/lists/{id}/accounts", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/lists/{id}/accounts", placeholderEndpoint)
	router.HandleFunc("GET /v1/markers", placeholderEndpoint)
	router.HandleFunc("POST /v1/markers", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/health", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/user", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/user/notification", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/public", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/public/local", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/public/remote", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/hashtag", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/hashtag/local", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/list", placeholderEndpoint)
	router.HandleFunc("GET /v1/streaming/direct", placeholderEndpoint)
	router.HandleFunc("GET /v2/notifications", placeholderEndpoint)
	router.HandleFunc("GET /v2/notifications/:group_key", placeholderEndpoint)
	router.HandleFunc("POST /v2/notifications/:group_key/dismiss", placeholderEndpoint)
	router.HandleFunc("GET /v2/notifications/:group_key/accounts", placeholderEndpoint)
	router.HandleFunc("GET /v2/notifications/unread_count", placeholderEndpoint)
	router.HandleFunc("GET /v1/notifications", placeholderEndpoint)
	router.HandleFunc("GET /v1/notifications/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/notifications/clear", placeholderEndpoint)
	router.HandleFunc("POST /v1/notifications/{id}/dismiss", placeholderEndpoint)
	router.HandleFunc("GET /v1/notifications/unread_count", placeholderEndpoint)
	router.HandleFunc("GET /v2/notifications/policy", placeholderEndpoint)
	router.HandleFunc("PATCH /v2/notifications/policy", placeholderEndpoint)
	router.HandleFunc("GET /v1/notifications/requests", placeholderEndpoint)
	router.HandleFunc("GET /v1/notifications/requests/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/notifications/requests/{id}/accept", placeholderEndpoint)
	router.HandleFunc("POST /v1/notifications/requests/{id}/dismiss", placeholderEndpoint)
	router.HandleFunc("POST /v1/notifications/requests/accept", placeholderEndpoint)
	router.HandleFunc("POST /v1/notifications/requests/dismiss", placeholderEndpoint)
	router.HandleFunc("GET /v1/notifications/requests/merged", placeholderEndpoint)
	router.HandleFunc("POST /v1/push/subscription", placeholderEndpoint)
	router.HandleFunc("GET /v1/push/subscription", placeholderEndpoint)
	router.HandleFunc("PUT /v1/push/subscription", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/push/subscription", placeholderEndpoint)
	router.HandleFunc("GET /v2/search", placeholderEndpoint)
	router.HandleFunc("GET /v2/instance", placeholderEndpoint)
	router.HandleFunc("GET /v1/instance/peers", placeholderEndpoint)
	router.HandleFunc("GET /v1/instance/activity", placeholderEndpoint)
	router.HandleFunc("GET /v1/instance/rules", placeholderEndpoint)
	router.HandleFunc("GET /v1/instance/domain_blocks", placeholderEndpoint)
	router.HandleFunc("GET /v1/instance/extended_description", placeholderEndpoint)
	router.HandleFunc("GET /v1/instance/translation_languages", placeholderEndpoint)
	router.HandleFunc("GET /v1/trends/tags", placeholderEndpoint)
	router.HandleFunc("GET /v1/trends/statuses", placeholderEndpoint)
	router.HandleFunc("GET /v1/trends/links", placeholderEndpoint)
	router.HandleFunc("GET /v1/directory", placeholderEndpoint)
	router.HandleFunc("GET /v1/custom_emojis", placeholderEndpoint)
	router.HandleFunc("GET /v1/announcements", placeholderEndpoint)
	router.HandleFunc("POST /v1/announcements/{id}/dismiss", placeholderEndpoint)
	router.HandleFunc("PUT /v1/announcements/{id}/reactions/:name", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/announcements/{id}/reactions/:name", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/accounts", placeholderEndpoint)
	router.HandleFunc("GET /v2/admin/accounts", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/accounts/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/accounts/{id}/approve", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/accounts/{id}/reject", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/admin/accounts/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/accounts/{id}/action", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/accounts/{id}/enable", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/accounts/{id}/unsilence", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/accounts/{id}/unsuspend", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/accounts/{id}/unsensitive", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/canonical_email_blocks", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/canonical_email_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/canonical_email_blocks/test", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/canonical_email_blocks", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/admin/canonical_email_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/dimensions", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/domain_allows", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/domain_allows/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/domain_allows", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/admin/domain_allows/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/domain_blocks", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/domain_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/domain_blocks", placeholderEndpoint)
	router.HandleFunc("PUT /v1/admin/domain_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/admin/domain_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/email_domain_blocks", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/email_domain_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/email_domain_blocks", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/admin/email_domain_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/ip_blocks", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/ip_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/ip_blocks", placeholderEndpoint)
	router.HandleFunc("PUT /v1/admin/ip_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("DELETE /v1/admin/ip_blocks/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/measures", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/reports", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/reports/{id}", placeholderEndpoint)
	router.HandleFunc("PUT /v1/admin/reports/{id}", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/reports/{id}/assign_to_self", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/reports/{id}/unassign", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/reports/{id}/resolve", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/reports/{id}/reopen", placeholderEndpoint)
	router.HandleFunc("POST /v1/admin/retention", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/trends/links", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/trends/statuses", placeholderEndpoint)
	router.HandleFunc("GET /v1/admin/trends/tags", placeholderEndpoint)
	router.HandleFunc("GET /oembed", placeholderEndpoint)

	router.HandleFunc(
		"GET /v1/accounts/{id}/identity_proofs",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"GET /v1/filters",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"GET /v1/filters/{id}",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"POST /v1/filters",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"PUT /v1/filters/{id}",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"DELETE /v1/filters/{id}",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"GET /v1/suggestions",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"GET /v1/statuses/{id}/card",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"POST /v1/media",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"GET /v1/timelines/direct",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"POST /v1/notifications/dismiss",
		placeholderEndpoint,
	) // Removed
	router.HandleFunc(
		"GET /v1/search",
		placeholderEndpoint,
	) // Removed
	router.HandleFunc(
		"GET /v1/instance",
		placeholderEndpoint,
	) // Deprecated
	router.HandleFunc(
		"GET /proofs",
		placeholderEndpoint,
	) // Removed

	return router
}
