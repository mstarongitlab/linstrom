package server

import (
	"fmt"
	"net/http"
)

func setupTestEndpoints() http.Handler {
	router := http.NewServeMux()
	router.HandleFunc(
		"/",
		func(w http.ResponseWriter, r *http.Request) { fmt.Fprint(w, "test root") },
	)

	return router
}
