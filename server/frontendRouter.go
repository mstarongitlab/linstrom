package server

import (
	"io/fs"
	"net/http"
)

// Mounted at /
func setupFrontendRouter(interactiveFs, noscriptFs fs.FS) http.Handler {
	router := http.NewServeMux()
	router.Handle("/noscript/", http.StripPrefix("/noscript", http.FileServerFS(noscriptFs)))
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFileFS(w, r, interactiveFs, "index.html")
	})
	router.Handle("/assets/", http.FileServerFS(interactiveFs))
	router.HandleFunc(
		"/robots.txt",
		func(w http.ResponseWriter, r *http.Request) { http.ServeFileFS(w, r, interactiveFs, "robots.txt") },
	)

	return router
}
