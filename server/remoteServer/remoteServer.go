package remotestorage

import "gitlab.com/mstarongitlab/linstrom/storage"

// Wrapper around db storage
// storage.Storage is for the db and cache access only,
// while this one wraps storage.Storage to also provide remote fetching of missing resources.
// So if an account doesn't exist in db or cache, this wrapper will attempt to fetch it
type RemoteStorage struct {
	store *storage.Storage
}

// TODO: Implement just about everything storage has, but with remote fetching if storage fails
