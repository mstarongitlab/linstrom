package server

// Contains types used by the Linstrom API. Types comply with the jsonapi spec

import "time"

var (
	_ = linstromRole{}
	_ = linstromRelation{}
)

type linstromNote struct {
	Id             string                   `jsonapi:"primary,notes"`
	RawContent     string                   `jsonapi:"attr,content"`
	OriginServer   *linstromOriginServer    `jsonapi:"relation,origin-server"`
	OriginServerId int                      `jsonapi:"attr,origin-server-id"`
	ReactionCount  string                   `jsonapi:"attr,reaction-count"`
	CreatedAt      time.Time                `jsonapi:"attr,created-at"`
	UpdatedAt      *time.Time               `jsonapi:"attr,updated-at,omitempty"`
	Author         *linstromAccount         `jsonapi:"relation,author"`
	AuthorId       string                   `jsonapi:"attr,author-id"`
	ContentWarning *string                  `jsonapi:"attr,content-warning,omitempty"`
	InReplyToId    *string                  `jsonapi:"attr,in-reply-to-id,omitempty"`
	QuotesId       *string                  `jsonapi:"attr,quotes-id,omitempty"`
	EmoteIds       []string                 `jsonapi:"attr,emotes,omitempty"`
	Attachments    []*linstromMediaMetadata `jsonapi:"relation,attachments,omitempty"`
	AttachmentIds  []string                 `jsonapi:"attr,attachment-ids"`
	AccessLevel    uint8                    `jsonapi:"attr,access-level"`
	Pings          []*linstromAccount       `jsonapi:"relation,pings,omitempty"`
	PingIds        []string                 `jsonapi:"attr,ping-ids,omitempty"`
	ReactionIds    []uint                   `jsonapi:"attr,reaction-ids"`
}

type linstromOriginServer struct {
	Id          uint                   `jsonapi:"primary,origins"`
	CreatedAt   time.Time              `jsonapi:"attr,created-at"`
	UpdatedAt   *time.Time             `jsonapi:"attr,updated-at,omitempty"`
	ServerType  string                 `jsonapi:"attr,server-type"` // one of "Linstrom", "Mastodon", "Plemora", "Misskey" or "Wafrn"
	Domain      string                 `jsonapi:"attr,domain"`
	DisplayName string                 `jsonapi:"attr,display-name"`
	Icon        *linstromMediaMetadata `jsonapi:"relation,icon"`
	IsSelf      bool                   `jsonapi:"attr,is-self"`
}

type linstromMediaMetadata struct {
	Id        string     `jsonapi:"primary,media"`
	CreatedAt time.Time  `jsonapi:"attr,created-at"`
	UpdatedAt *time.Time `jsonapi:"attr,updated-at,omitempty"`
	IsRemote  bool       `jsonapi:"attr,is-remote"`
	Url       string     `jsonapi:"attr,url"`
	MimeType  string     `jsonapi:"attr,mime-type"`
	Name      string     `jsonapi:"attr,name"`
	AltText   string     `jsonapi:"attr,alt-text"`
	Blurred   bool       `jsonapi:"attr,blurred"`
}

type linstromAccount struct {
	Id               string                        `jsonapi:"primary,accounts"`
	CreatedAt        time.Time                     `jsonapi:"attr,created-at"`
	UpdatedAt        *time.Time                    `jsonapi:"attr,updated-at,omitempty"`
	Username         string                        `jsonapi:"attr,username"`
	OriginServer     *linstromOriginServer         `jsonapi:"relation,origin-server"`
	OriginServerId   int                           `jsonapi:"attr,origin-server-id"`
	DisplayName      string                        `jsonapi:"attr,display-name"`
	CustomFields     []*linstromCustomAccountField `jsonapi:"relation,custom-fields"`
	CustomFieldIds   []uint                        `jsonapi:"attr,custom-field-ids"`
	IsBot            bool                          `jsonapi:"attr,is-bot"`
	Description      string                        `jsonapi:"attr,description"`
	Icon             *linstromMediaMetadata        `jsonapi:"relation,icon"`
	IconId           string                        `jsonapi:"attr,icon-id"`
	Banner           *linstromMediaMetadata        `jsonapi:"relation,banner"`
	BannerId         *string                       `jsonapi:"attr,banner-id"`
	Background       *linstromMediaMetadata        `jsonapi:"relation,background"`
	BackgroundId     *string                       `jsonapi:"attr,background-id"`
	RelationIds      []uint                        `jsonapi:"attr,follows-ids"`
	Indexable        bool                          `jsonapi:"attr,indexable"`
	RestrictedFollow bool                          `jsonapi:"attr,restricted-follow"`
	IdentifiesAs     []string                      `jsonapi:"attr,identifies-as"`
	Pronouns         []string                      `jsonapi:"attr,pronouns"`
	Roles            []string                      `jsonapi:"attr,roles"`
}

type linstromCustomAccountField struct {
	Id          uint       `jsonapi:"primary,custom-account-fields"`
	CreatedAt   time.Time  `jsonapi:"attr,created-at"`
	UpdatedAt   *time.Time `jsonapi:"attr,updated-at,omitempty"`
	Key         string     `jsonapi:"attr,key"`
	Value       string     `jsonapi:"attr,value"`
	Verified    *bool      `jsonapi:"attr,verified,omitempty"`
	BelongsToId string     `jsonapi:"attr,belongs-to-id"`
}

type linstromRelation struct {
	Id        uint      `jsonapi:"primary,relations"`
	CreatedAt time.Time `jsonapi:"attr,created-at"`
	UpdatedAt time.Time `jsonapi:"attr,updated-at"`
	FromId    string    `jsonapi:"attr,from-id"`
	ToId      string    `jsonapi:"attr,to-id"`
	Requested bool      `jsonapi:"attr,requested"`
	Accepted  bool      `jsonapi:"attr,accepted"`
}

type linstromReaction struct {
	Id        uint           `jsonapi:"primary,reactions"`
	NoteId    string         `jsonapi:"attr,note-id"`
	ReactorId string         `jsonapi:"attr,reactor-id"`
	EmoteId   uint           `jsonapi:"attr,emote-id"`
	Emote     *linstromEmote `jsonapi:"relation,emote"`
}

type linstromEmote struct {
	Id         uint                   `jsonapi:"primary,emotes"`
	MetadataId string                 `jsonapi:"attr,metadata-id"`
	Metadata   *linstromMediaMetadata `jsonapi:"relation,metadata"`
	Name       string                 `jsonapi:"attr,name"`
	ServerId   uint                   `jsonapi:"attr,server-id"`
	Server     *linstromOriginServer  `jsonapi:"relation,server"`
}
