package server

import (
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/hlog"
)

// TODO: Decide where to put data stream handlers

var websocketUpgrader = websocket.Upgrader{}

// Entrypoint for a new stream will be in here at least
func linstromEventStream(w http.ResponseWriter, r *http.Request) {
	log := hlog.FromRequest(r)
	conn, err := websocketUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Warn().Err(err).Msg("Failed to upgrade connection to websocket")
	}
	defer conn.Close()
	// TODO: Handle initial request for what events to receive
	// TODO: Stream all requested events until connection closes (due to bad data from client or disconnect)
}
