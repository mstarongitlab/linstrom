package mailer

import (
	"io/fs"

	mail "github.com/xhit/go-simple-mail/v2"
)

type MailClient struct {
	mailServer  *mail.SMTPServer
	templatesFs fs.FS
}
