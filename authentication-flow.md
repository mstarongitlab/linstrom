# Plan for how authentication will work

## Frontend auth

### Registration

1. Send username to registration endpoint
2. Get webauthn options
3. Perform webauthn check (selecting and confirming passkey)
4. Server verifies response
5. Minimal account ready for login

### Login

1. Send username to login endpoint
2. Error out if user doesn't exist
3. Get webauthn options from response
4. Get passkey response
5. Send response to Server
6. Server checks and gives ok or fail.
   Ok also sets a server only cookie with an access token

## api

1. Generate API token via frontend
2. Use api token for authorisation
