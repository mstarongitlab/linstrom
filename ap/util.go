package ap

import "strings"

type InvalidFullHandleError struct {
	Raw string
}

func (i InvalidFullHandleError) Error() string {
	return "Invalid full handle"
}

func SplitFullHandle(full string) (string, string, error) {
	splits := strings.Split(strings.TrimPrefix(full, "@"), "@")
	if len(splits) != 2 {
		return "", "", InvalidFullHandleError{}
	}
	return splits[0], splits[1], nil
}
