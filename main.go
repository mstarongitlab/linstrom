// TODO: Add EUPL banner everywhere
package main

import (
	"embed"
	"io"
	"os"
	"strings"
	"time"

	"github.com/go-webauthn/webauthn/webauthn"
	"github.com/mstarongithub/passkey"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	// "gitlab.com/mstarongitlab/linstrom/ap"
	"gitlab.com/mstarongitlab/linstrom/config"
	"gitlab.com/mstarongitlab/linstrom/server"
	"gitlab.com/mstarongitlab/linstrom/storage"
	"gitlab.com/mstarongitlab/linstrom/storage/cache"
	"gitlab.com/mstarongitlab/linstrom/util"
)

//go:embed frontend-reactive/dist/* frontend-reactive/dist/assets
var reactiveFS embed.FS

//go:embed frontend-noscript
var nojsFS embed.FS

//go:embed duck.webp
var placeholderFile string

func main() {
	setLogger()
	setLogLevel()
	if err := config.ReadAndWriteToGlobal(*flagConfigFile); err != nil {
		log.Fatal().
			Err(err).
			Str("config-file", *flagConfigFile).
			Msg("Failed to read config and couldn't write default")
	}

	// Request to only check config
	if *flagConfigOnly {
		return
	}

	storageCache, err := cache.NewCache(
		config.GlobalConfig.Storage.MaxInMemoryCacheSize,
		config.GlobalConfig.Storage.RedisUrl,
	)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to start cache")
	}

	// var store *storage.Storage
	// if config.GlobalConfig.Storage.DbIsPostgres != nil && *config.GlobalConfig.Storage.DbIsPostgres {
	// 	store, err = storage.NewStoragePostgres(config.GlobalConfig.Storage.DatabaseUrl, storageCache)
	// } else {
	// 	store, err = storage.NewStorageSqlite(config.GlobalConfig.Storage.DatabaseUrl, storageCache)
	// }
	//
	store, err := storage.NewStorage(config.GlobalConfig.Storage.BuildPostgresDSN(), storageCache)

	if err != nil {
		log.Fatal().Err(err).Msg("Failed to setup storage")
	}

	pkey, err := passkey.New(passkey.Config{
		WebauthnConfig: &webauthn.Config{
			RPDisplayName: "Linstrom",
			RPID:          "localhost",
			RPOrigins:     []string{"http://localhost:8000"},
		},
		UserStore:     store,
		SessionStore:  store,
		SessionMaxAge: time.Hour * 24,
	}, passkey.WithLogger(&util.ZerologWrapper{}))
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to setup passkey support")
	}

	server := server.NewServer(
		store,
		pkey,
		util.NewFSWrapper(reactiveFS, "frontend-reactive/dist/", false),
		util.NewFSWrapper(nojsFS, "frontend-noscript/", false),
		&placeholderFile,
	)
	server.Start(":8000")
	// TODO: Set up media server
	// TODO: Set up queues
	// TODO: Set up http server
	// TODO: Set up plugins
	// TODO: Start everything
}

func setLogLevel() {
	log.Info().Str("new-level", *flagLogLevel).Msg("Attempting to set log level")
	switch strings.ToLower(*flagLogLevel) {
	case "trace":
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "warn":
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case "fatal":
		zerolog.SetGlobalLevel(zerolog.FatalLevel)
	default:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}
}

func setLogger(extraLogWriters ...io.Writer) {
	if *flagPrettyPrint {
		console := zerolog.ConsoleWriter{Out: os.Stderr}
		log.Logger = zerolog.New(zerolog.MultiLevelWriter(append([]io.Writer{console}, extraLogWriters...)...)).
			With().
			Timestamp().
			Logger()
	} else {
		log.Logger = zerolog.New(zerolog.MultiLevelWriter(
			append([]io.Writer{log.Logger}, extraLogWriters...)...,
		)).With().Timestamp().Logger()
	}
}
