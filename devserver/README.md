All services for running a Linstrom dev server can be found in the docker compose file

Garage (s3 provider) isn't fully setup in the compose file, see [https://garagehq.deuxfleurs.fr/documentation/quick-start/]
for a guide on setting it up

If you don't want to install the garage app on your system, you can use the one in
the container via `docker exec -it devserver-s3-1 /garage <args>`
