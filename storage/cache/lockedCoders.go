package cache

import (
	"bytes"
	"encoding/gob"
	"sync"
)

type gobEncoder struct {
	sync.Mutex
	Encoder *gob.Encoder
	Buffer  *bytes.Buffer
}

func newEncoder() gobEncoder {
	buf := bytes.Buffer{}
	return gobEncoder{
		Encoder: gob.NewEncoder(&buf),
		Buffer:  &buf,
	}
}

type gobDecoder struct {
	sync.Mutex
	Decoder *gob.Decoder
	Buffer  *bytes.Buffer
}

func newDecoder() gobDecoder {
	buf := bytes.Buffer{}
	return gobDecoder{
		Decoder: gob.NewDecoder(&buf),
		Buffer:  &buf,
	}
}
