package storage

import (
	"database/sql/driver"
	"errors"
)

// TODO: Decide whether to turn this into an int too to save resources
// And then use go:generate instead for pretty printing

// What software a server is running
// Mostly important for rendering
type RemoteServerType string

const (
	// Includes forks like glitch-soc, etc
	REMOTE_SERVER_MASTODON = RemoteServerType("Mastodon")
	// Includes forks like Ice Shrimp, Sharkey, Cutiekey, etc
	REMOTE_SERVER_MISSKEY = RemoteServerType("Misskey")
	// Includes Akkoma
	REMOTE_SERVER_PLEMORA = RemoteServerType("Plemora")
	// Wafrn is a new entry
	REMOTE_SERVER_WAFRN = RemoteServerType("Wafrn")
	// And of course, yours truly
	REMOTE_SERVER_LINSTROM = RemoteServerType("Linstrom")
)

func (r *RemoteServerType) Value() (driver.Value, error) {
	return r, nil
}

func (r *RemoteServerType) Scan(raw any) error {
	if v, ok := raw.(string); ok {
		*r = RemoteServerType(v)
		return nil
	} else {
		return errors.New("value not a string")
	}
}
