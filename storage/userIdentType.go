package storage

import "gitlab.com/mstarongitlab/goutils/sliceutils"

// What kind of being a user identifies as
type Being string

const (
	BEING_HUMAN = Being("human")
	BEING_CAT   = Being("cat")
	BEING_FOX   = Being("fox")
	BEING_DOG   = Being("dog")
	BEING_ROBOT = Being("robot")
	BEING_DOLL  = Being("doll")
)

var allBeings = []Being{BEING_HUMAN, BEING_CAT, BEING_FOX, BEING_DOG, BEING_ROBOT, BEING_DOLL}

func IsValidBeing(toCheck string) bool {
	return sliceutils.Contains(allBeings, Being(toCheck))
}
