package storage

import "gorm.io/gorm"

type Emote struct {
	gorm.Model
	// Metadata   MediaMetadata // `gorm:"foreignKey:MetadataId"`
	MetadataId string
	Name       string
	// Server     RemoteServer // `gorm:"foreignKey:ServerId;references:ID"`
	ServerId uint
}

func (s *Storage) GetEmoteById(id uint) (*Emote, error) {
	out := Emote{}
	err := s.db.First(&out, id).Error
	switch err {
	case nil:
		return &out, nil
	case gorm.ErrRecordNotFound:
		return nil, ErrEntryNotFound
	default:
		return nil, err
	}
}
