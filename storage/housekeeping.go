package storage

// Contains various functions for housekeeping
// Things like true deletion of soft deleted data after some time
// Or removing inactive access tokens
// All of this will be handled by goroutines

// TODO: Delete everything soft deleted and older than a month
// TODO: Delete old tokens not in active use anymore
// TODO: Start jobs where the last check-in was more than an hour ago
//
