package storage

import "errors"

type ErrNotImplemented struct{}

func (n ErrNotImplemented) Error() string {
	return "Not implemented yet"
}

var ErrEntryNotFound = errors.New("entry not found")
var ErrEntryAlreadyExists = errors.New("entry already exists")
var ErrNothingToChange = errors.New("nothing to change")
var ErrInvalidData = errors.New("invalid data")
var ErrNotAllowed = errors.New("action not allowed")
