package storage

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/mstarongitlab/linstrom/util"
	"gorm.io/gorm"
)

type OutboundJob struct {
	gorm.Model // Include full model. Gives ID, created and updated at timestamps as well as soft deletes
	// Read (and create) only values to ensure consistency
	TargetServer string `gorm:"->;<-:create"` // The url of the target server
	TargetPath   string `gorm:"->;<-:create"` // The full path of api endpoint targeted
	Data         []byte `gorm:"->;<-:create"` // The raw data to send
}

func (s *Storage) AddNewOutboundJob(data []byte, targetDomain string, targetUrl string) {
	defer util.Untrace(util.Trace(&log.Logger))
	newJob := OutboundJob{
		Data:         data,
		TargetServer: targetDomain,
		TargetPath:   targetUrl,
	}
	s.db.Create(&newJob)
}

// Get the specified amount of jobs, sorted by age (oldest first)
func (s *Storage) GetOldestOutboundJobs(amount uint) ([]OutboundJob, error) {
	defer util.Untrace(util.Trace(&log.Logger))
	jobs := []OutboundJob{}
	err := s.db.Order("id asc, created_at asc").Limit(int(amount)).Find(jobs).Error
	switch err {
	case gorm.ErrRecordNotFound:
		return nil, ErrEntryNotFound
	case nil:
		return jobs, nil
	default:
		return nil, err
	}
}

func (s *Storage) GetOutboundJobsForDomain(domain string, amount uint) ([]OutboundJob, error) {
	defer util.Untrace(util.Trace(&log.Logger))
	jobs := []OutboundJob{}
	err := s.db.Where("target_server = ?", domain).
		Order("id asc, created_at asc").
		Limit(int(amount)).
		Find(jobs).
		Error
	switch err {
	case gorm.ErrRecordNotFound:
		return nil, ErrEntryNotFound
	case nil:
		return jobs, nil
	default:
		return nil, err
	}
}

func (s *Storage) CompleteOutboundJob(id uint) error {
	defer util.Untrace(util.Trace(&log.Logger))
	s.db.Delete(OutboundJob{Model: gorm.Model{ID: id}})
	return nil
}
