package storage

import (
	"gorm.io/gorm"
)

type AccountRelation struct {
	gorm.Model
	FromId   string
	ToId     string
	Accepted bool
}

func (s *Storage) GetRelationBetween(fromId, toId string) (*AccountRelation, error) {
	rel := AccountRelation{}
	err := s.db.Where(AccountRelation{FromId: fromId, ToId: toId}).First(&rel).Error
	switch err {
	case gorm.ErrRecordNotFound:
		return nil, ErrEntryNotFound
	case nil:
		return &rel, nil
	default:
		return nil, err
	}
}
