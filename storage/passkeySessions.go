package storage

import (
	"github.com/go-webauthn/webauthn/webauthn"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gitlab.com/mstarongitlab/linstrom/util"
)

// Session data used during login attempts with a passkey
// Not actually used afterwards to verify a normal session
// NOTE: Doesn't contain a DeletedAt field, thus deletions are automatically hard and not reversible
type PasskeySession struct {
	ID   string               `gorm:"primarykey"`
	Data webauthn.SessionData `gorm:"serializer:json"`
}

// ---- Section SessionStore

// Generate some id for a new session. Just returns a new uuid
func (s *Storage) GenSessionID() (string, error) {
	defer util.Untrace(util.Trace(&log.Logger))
	x := uuid.NewString()
	log.Debug().Str("session-id", x).Msg("Generated new passkey session id")
	return x, nil
}

// Look for an active session with a given id
// Returns the session if found and a bool indicating if a session was found
func (s *Storage) GetSession(sessionId string) (*webauthn.SessionData, bool) {
	defer util.Untrace(util.Trace(&log.Logger))
	log.Debug().Str("id", sessionId).Msg("Looking for passkey session")
	session := PasskeySession{}
	res := s.db.Where("id = ?", sessionId).First(&session)
	if res.Error != nil {
		return nil, false
	}
	log.Debug().Str("id", sessionId).Any("webauthn-data", &session).Msg("Found passkey session")
	return &session.Data, true
}

// Save (or update) a session with the new data
func (s *Storage) SaveSession(token string, data *webauthn.SessionData) {
	defer util.Untrace(util.Trace(&log.Logger))
	log.Debug().Str("id", token).Any("webauthn-data", data).Msg("Saving passkey session")
	session := PasskeySession{
		ID:   token,
		Data: *data,
	}
	s.db.Save(&session)
}

// Delete a session
// NOTE: This is a hard delete since the session struct contains no DeletedAt field
func (s *Storage) DeleteSession(token string) {
	defer util.Untrace(util.Trace(&log.Logger))
	log.Debug().Str("id", token).Msg("Deleting passkey session (if one exists)")
	s.db.Delete(&PasskeySession{ID: token})
}
