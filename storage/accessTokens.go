package storage

import (
	"time"

	"gorm.io/gorm"
)

type AccessToken struct {
	gorm.Model
	BelongsToUserId string
	Name            string
	Token           string
	ExpiresAt       time.Time
}

func (s *Storage) GetTokensForAccId(accId uint) ([]AccessToken, error) {
	// TODO: Implement me
	panic("Not implemented")
}

func (s *Storage) NewAccessToken(
	forAccId uint,
	name string,
	expiresAt time.Time,
) (*AccessToken, error) {
	// TODO: Implement me
	panic("Not implemented")
}

func (s *Storage) ExtendToken(accId uint, newExpiry time.Time) error {
	// TODO: Implement me
	panic("Not implemented")
}

func (s *Storage) RenameToken(accId, oldName string, newName string) error {
	// TODO: Implement me
	panic("Not implemented")
}

func (s *Storage) DiscardToken(accId uint, name string) error {
	// TODO: Implement me
	panic("Not implemented")
}
