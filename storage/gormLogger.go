package storage

import (
	"context"
	"time"

	"github.com/rs/zerolog"
	"gorm.io/gorm/logger"
)

type gormLogger struct {
	logger zerolog.Logger
}

func newGormLogger(zerologger zerolog.Logger) *gormLogger {
	return &gormLogger{zerologger}
}

func (g *gormLogger) LogMode(newLevel logger.LogLevel) logger.Interface {
	switch newLevel {
	case logger.Error:
		g.logger = g.logger.Level(zerolog.ErrorLevel)
	case logger.Warn:
		g.logger = g.logger.Level(zerolog.WarnLevel)
	case logger.Info:
		g.logger = g.logger.Level(zerolog.InfoLevel)
	case logger.Silent:
		g.logger = g.logger.Level(zerolog.Disabled)
	}
	return g
}
func (g *gormLogger) Info(ctx context.Context, format string, args ...interface{}) {
	g.logger.Info().Ctx(ctx).Msgf(format, args...)
}
func (g *gormLogger) Warn(ctx context.Context, format string, args ...interface{}) {
	g.logger.Warn().Ctx(ctx).Msgf(format, args...)
}
func (g *gormLogger) Error(ctx context.Context, format string, args ...interface{}) {
	g.logger.Error().Ctx(ctx).Msgf(format, args...)
}

func (g *gormLogger) Trace(
	ctx context.Context,
	begin time.Time,
	fc func() (sql string, rowsAffected int64),
	err error,
) {
	sql, rowsAffected := fc()
	g.logger.Trace().
		Ctx(ctx).
		Time("gorm-begin", begin).
		Err(err).
		Str("gorm-query", sql).
		Int64("gorm-rows-affected", rowsAffected).
		Send()
}

func (g *gormLogger) OverwriteLoggingLevel(new zerolog.Level) {
	g.logger = g.logger.Level(new)
}

func (g *gormLogger) OverwriteLogger(new zerolog.Logger) {
	g.logger = new
}
