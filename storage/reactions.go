package storage

import "gorm.io/gorm"

type Reaction struct {
	gorm.Model
	NoteId    string
	ReactorId string
	EmoteId   uint
}
