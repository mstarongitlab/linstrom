package storage

type AccountRestriction int64

const (
	// Account has no restrictions applied
	ACCOUNT_RESTRICTION_NONE = AccountRestriction(0)
	// All messages of the account get a content warning applied if none is set
	// Warning could be something like "Message auto-CWd by server"
	ACCOUNT_RESTRICTION_AUTO_CW = AccountRestriction(1 << iota)
	// Disable accessing the account via login or access token
	ACCOUNT_RESTRICTION_DISABLE_LOGIN
	// Disable sending activities to other servers if the account is local
	// Or reject all activities if the account is remote
	ACCOUNT_RESTRICTION_NO_FEDERATION
	// Disallow sending direct messages from that account
	ACCOUNT_RESTRICTION_NO_DMS
	// Disallow sending follower only messages from that account
	ACCOUNT_RESTRICTION_NO_FOLLOWER_POSTS
	// Disable outbound follow requests (restricted account can't send follow requests)
	ACCOUNT_RESTRICTION_DISABLE_OUTBOUND_FOLLOWS
	// Disable inbound follow requests (all follow requests to that account are automatically rejected)
	ACCOUNT_RESTRICTION_DISABLE_INBOUND_FOLLOWS
	// Forces all posts by that account to be follower only
	ACCOUNT_RESTRICTION_FORCE_FOLLOWERS_ONLY
	// Disable all outbound activities of an account.
	// Includes sending, updating or deleting own notes
	// as well as boosting or reacting to any notes
	ACCOUNT_RESTRICTION_DISABLE_ACTIVITIES
	// Account can only be viewed while logged in or via verified requests to the AP endpoints
	ACCOUNT_RESTRICTIONS_NO_PUBLIC_ACCESS
	// Force tag all media the account posts as sensitive
	ACCOUNT_RESTRICTIONS_FORCE_MEDIA_SENSITIVE
)
