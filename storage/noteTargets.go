package storage

import (
	"database/sql/driver"
	"errors"
)

// For pretty printing during debug
// If `go generate` is run, it'll generate the necessary function and data for pretty printing
//go:generate stringer -type NoteAccessLevel

// What feed a note is targeting (public, home, followers or dm)
type NoteAccessLevel uint8

const (
	// The note is intended for the public
	NOTE_TARGET_PUBLIC NoteAccessLevel = 0
	// The note is intended only for the home screen
	// not really any idea what the difference is compared to public
	// Maybe home notes don't show up on the server feed but still for everyone's home feed if it reaches them via follow or boost
	NOTE_TARGET_HOME NoteAccessLevel = 1 << iota
	// The note is intended only for followers
	NOTE_TARGET_FOLLOWERS
	// The note is intended only for a DM to one or more targets
	NOTE_TARGET_DM
)

// Converts the NoteTarget value into a type the DB can use
func (n *NoteAccessLevel) Value() (driver.Value, error) {
	return n, nil
}

// Converts the raw value from the DB into a NoteTarget
func (n *NoteAccessLevel) Scan(value any) error {
	vBig, ok := value.(int64)
	if !ok {
		return errors.New("not an int64")
	}
	*n = NoteAccessLevel(vBig)
	return nil
}
