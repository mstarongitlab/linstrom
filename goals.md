## Easy
- optional content filter with Microsoft's ai scan thing (user and server level)
- lockdown mode (all incoming stuff will be bonked immediately) (user and server)
- Post highlighting (opposite of muting) where if a post contains some specific thing, it gets some highlight
  - Maybe even with different highlighting options

## Medium
- optional automatic server screening
- metadata sharing (thing like link previews or blocklists)
- asks (in some way that is compatible with wafrn hopefully)
- rss feed imports
- Database converter (Masto/Akoma/Mk -> Linstrom, maybe also other way around)

## Hard
- custom "ads" created and controlled by server admins
- some sort of subscription/payment system (opt-in (you have to opt in to potentially see monetised stuff in the first place))
- extended account moderation (user and server)
- custom api for working around AP being a pos:
  - includes messages always being encrypted
  - bunch of other optimisations

# Variable difficutly
- Multiple built-in frontends
  - Primary using ember, focus on good looking and most feature complete
  - Modifyable using htmx (not sure on this one yet)
  - Low resource/no script with pure html and Go templating
