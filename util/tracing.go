package util

import (
	"github.com/rs/zerolog"
)

func Trace(l *zerolog.Logger) *zerolog.Logger {
	if e := l.Trace(); e.Enabled() {
		e.Caller(2).
			Msg("Entered function")
	}
	return l
}

func Untrace(l *zerolog.Logger) {
	if e := l.Trace(); e.Enabled() {
		e.Caller(2).
			Msg("Exited function")
	}
}
