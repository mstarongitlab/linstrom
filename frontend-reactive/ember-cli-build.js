'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function (defaults) {
  const app = new EmberApp(defaults, {
    'ember-cli-babel': { enableTypeScriptTransform: true },
    minifyCSS: {
      options: { processImport: true },
    },

    // Add options here
    autoImport: {
      watchDependencies: ['ember-moment'],
    },
  });

  return app.toTree();
};
