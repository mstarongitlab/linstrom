import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-reactive/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | note', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });
    this.set('note', {
      displayname: 'bob',
      username: 'alice',
      server: 'example.com',
      content: 'some content',
      createdAt: Date.now(),
    });

    await render(hbs`
      <Note @note={{this.note}}/>
    `);

    assert.dom('p.note-user-displayname').hasText('bob');
    assert.dom('p.note-user-handle').hasText('@alice@example.com');
    assert.dom('p.note-content-text').hasText('some content');
    // TODO: Fix tests
  });
});
