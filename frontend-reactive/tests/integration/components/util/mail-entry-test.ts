import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-reactive/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | util/mail-entry', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Util::MailEntry />`);

    assert.dom().hasText('');

    // Template block usage:
    await render(hbs`
      <Util::MailEntry>
        template block text
      </Util::MailEntry>
    `);

    assert.dom().hasText('template block text');
  });
});
