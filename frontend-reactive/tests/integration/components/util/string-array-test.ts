import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-reactive/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | util/string-array', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Util::StringArray />`);

    assert.dom().hasText('');

    // Template block usage:
    await render(hbs`
      <Util::StringArray>
        template block text
      </Util::StringArray>
    `);

    assert.dom().hasText('template block text');
  });
});
