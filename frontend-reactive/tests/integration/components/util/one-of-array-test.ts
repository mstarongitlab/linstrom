import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-reactive/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | util/one-of-array', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Util::OneOfArray />`);

    assert.dom().hasText('');

    // Template block usage:
    await render(hbs`
      <Util::OneOfArray>
        template block text
      </Util::OneOfArray>
    `);

    assert.dom().hasText('template block text');
  });
});
