import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-reactive/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | auth/registration-form', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Auth::PostRegistrationForm />`);

    assert.dom().hasText('');

    // Template block usage:
    await render(hbs`
      <Auth::PostRegistrationForm>
        template block text
      </Auth::PostRegistrationForm>
    `);

    assert.dom().hasText('template block text');
  });
});
