import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-reactive/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | note/user-header', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(
      hbs`<Note::UserHeader @displayname="bob" @handle="@alice@example.com"/>`,
    );

    assert.dom('p.note-user-displayname').hasText('bob');
    assert.dom('p.note-user-handle').hasText('@alice@example.com');
    // TODO: Expand tests to include profile picture
  });
});
