import { module, test } from 'qunit';
import { setupRenderingTest } from 'frontend-reactive/tests/helpers';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupIntl } from 'ember-intl/test-support';

module('Integration | Component | note/content', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks, 'en-us');

  test('keep short content as is', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    const shortContent = 'Lorem ipsum odor amet, consectetuer adipiscing elit.';
    this.set('shortContent', shortContent);

    await render(hbs`<Note::Content @content="{{this.shortContent}}"/>`);

    assert.dom('.note-content-text').hasText(shortContent);
  });

  test('long content cutoff and correct expansion', async function (assert) {
    const longContent =
      'Grapple keel reef fathom haul wind bilge rat swing the lead belay line pink. Man-of-war mizzenmast killick lookout yo-ho-ho Sail ho gabion careen sutler stern. Draught wherry lookout schooner prow hail-shot spanker Letter of Marque lateen sail strike colors.\n\nLad heave to topgallant scallywag scuppers Spanish Main poop deck spike hulk broadside. Snow take a caulk hornswaggle gaff swab quarter lugger spanker bilge provost. Man-of-war measured fer yer chains lugger cable loaded to the gunwalls prow piracy snow doubloon furl.\n\nDead men tell no tales jib chase guns gunwalls Gold Road smartly nipperkin topsail bilge water Pirate Round. Gaff gunwalls bilged on her anchor bilge water scourge of the seven seas parley ho sheet chase guns squiffy. Scuppers fathom ho quarter gally heave to yardarm coxswain red ensign pink.';
    this.set('longContent', longContent);

    await render(hbs`<Note::Content @content="{{this.longContent}}"/>`);

    assert
      .dom('.note-content-text')
      .hasText(
        'Grapple keel reef fathom haul wind bilge rat swing the lead belay line pink. Man-of-war mizzenmast killick lookout yo-ho-ho Sail ho gabion careen sutler stern. Draught wherry lookout schooner prow hail-shot ...',
      );
    assert.dom('.note-content-toggle').hasText('Expand');

    await click('.note-content-toggle');
    assert.dom('.note-content-toggle').hasText('Collapse');
    assert.dom('.note-content-text').hasText(longContent);
  });
});
