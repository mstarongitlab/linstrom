import { setupTest } from 'frontend-reactive/tests/helpers';
import { module, test } from 'qunit';

module('Unit | Model | reaction', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function (assert) {
    const store = this.owner.lookup('service:store');
    const model = store.createRecord('reaction', {});
    assert.ok(model, 'model exists');
  });
});
