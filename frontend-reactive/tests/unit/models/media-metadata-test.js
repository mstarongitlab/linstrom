import { setupTest } from 'frontend-reactive/tests/helpers';
import { module, test } from 'qunit';

module('Unit | Model | media metadata', function (hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function (assert) {
    const store = this.owner.lookup('service:store');
    const model = store.createRecord('media-metadata', {});
    assert.ok(model, 'model exists');
  });
});
