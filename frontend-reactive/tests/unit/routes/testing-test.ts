import { module, test } from 'qunit';
import { setupTest } from 'frontend-reactive/tests/helpers';

module('Unit | Route | testing', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    const route = this.owner.lookup('route:testing');
    assert.ok(route);
  });
});
