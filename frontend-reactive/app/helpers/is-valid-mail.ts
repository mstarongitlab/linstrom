import { helper } from '@ember/component/helper';

const re = /.+@\S+\.\S+/;

// Helper to check if a given email is *probably* valid
// Ofc, the only surefire way to check if an email exists is to send a test mail to it.
// This sending is expensive however, and thus some mostly sane defaults can be checked for
// beforehand. "Bananentürkis" for example is obviously not a valid address
export default helper(function isValidMail(positional: string[] /*, named*/) {
  for (const mail of positional) {
    if (!re.test(mail)) return false;
  }
  return true;
});
