import { helper } from '@ember/component/helper';

export default helper(function equals(args) {
  if (args.length != 2) return false;
  console.log(args[0], args[1]);
  return args[0] == args[1];
});
