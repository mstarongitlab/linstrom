import { helper } from '@ember/component/helper'

class Token {
  declare token: string
  declare elements: Array<string | Token>

  public constructor(token: string) {
    this.token = token
    this.elements = new Array()
  }
}

export default helper(function formatter(
  positional: Array<string>,
  named: {
    matchers: Array<{ match: RegExp; replace: string }>
  },
): string {
  let out = positional[0] ?? ''
  named.matchers.forEach((x) => {
    out = out.replaceAll(x.match, x.replace)
  })
  return out
})
