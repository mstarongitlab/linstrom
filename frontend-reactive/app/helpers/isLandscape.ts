export default function isLandscape(): boolean {
  return (
    Math.min(screen.availHeight, window.innerHeight) <
    Math.min(screen.availWidth, window.innerWidth)
  );
}
