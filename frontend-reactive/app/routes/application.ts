import Route from '@ember/routing/route'
import { type Registry as Services, service } from '@ember/service'

export default class ApplicationRoute extends Route {}
