import Route from '@ember/routing/route';

export default class RegisterFormRoute extends Route {
  async model() {
    return {
      list: [{ value: 'one' }, { value: 'two' }],
    };
  }
}
