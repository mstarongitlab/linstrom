import Route from '@ember/routing/route'
import { type Registry as Services, service } from '@ember/service'

export default class IndexRoute extends Route {
  @service declare intl: Services['intl']

  beforeModel() {
    this.intl.setLocale(['en-us'])
  }

  model() {
    console.log('root route loaded')
    return {
      notes: [
        {
          displayname: 'alice',
          username: 'bob',
          server: 'example.com',
          content: 'lorem ipsum',
          createdAt: Date.now() - 360000,
          editedAt: Date.now() - 60000,
          servertype: 'mastodon',
        },
        {
          displayname: 'Melody',
          username: 'mstar',
          server: 'woem.men',
          content:
            'Grapple keel reef fathom haul wind bilge rat swing the lead belay line pink. Man-of-war mizzenmast killick lookout yo-ho-ho Sail ho gabion careen sutler stern. Draught wherry lookout schooner prow hail-shot spanker Letter of Marque lateen sail strike colors.\n\nLad heave to topgallant scallywag scuppers Spanish Main poop deck spike hulk broadside. Snow take a caulk hornswaggle gaff swab quarter lugger spanker bilge provost. Man-of-war measured fer yer chains lugger cable loaded to the gunwalls prow piracy snow doubloon furl.\n\nDead men tell no tales jib chase guns gunwalls Gold Road smartly nipperkin topsail bilge water Pirate Round. Gaff gunwalls bilged on her anchor bilge water scourge of the seven seas parley ho sheet chase guns squiffy. Scuppers fathom ho quarter gally heave to yardarm coxswain red ensign pink.',
          createdAt: Date.now() - 3600,
          servertype: 'linstrom',
        },
        {
          displayname: 'alice',
          username: 'bob',
          server: 'example.com',
          content: 'lorem ipsum',
          createdAt: Date.now() - 360000,
          editedAt: Date.now() - 60000,
          servertype: 'wafrn',
        },
        {
          displayname: 'Melody',
          username: 'mstar',
          server: 'woem.men',
          content:
            'Grapple keel reef fathom haul wind bilge rat swing the lead belay line pink. Man-of-war mizzenmast killick lookout yo-ho-ho Sail ho gabion careen sutler stern. Draught wherry lookout schooner prow hail-shot spanker Letter of Marque lateen sail strike colors.\n\nLad heave to topgallant scallywag scuppers Spanish Main poop deck spike hulk broadside. Snow take a caulk hornswaggle gaff swab quarter lugger spanker bilge provost. Man-of-war measured fer yer chains lugger cable loaded to the gunwalls prow piracy snow doubloon furl.\n\nDead men tell no tales jib chase guns gunwalls Gold Road smartly nipperkin topsail bilge water Pirate Round. Gaff gunwalls bilged on her anchor bilge water scourge of the seven seas parley ho sheet chase guns squiffy. Scuppers fathom ho quarter gally heave to yardarm coxswain red ensign pink.',
          createdAt: Date.now() - 3600,
          servertype: 'unknown',
        },
      ],
    }
  }
}
