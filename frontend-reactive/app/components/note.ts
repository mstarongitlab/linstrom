import { action } from '@ember/object';
import Component from '@glimmer/component';

export interface NoteSignature {
  // The arguments accepted by the component
  Args: {
    isInTimeline: boolean;
    note: {
      content: string;
      server: string;
      username: string;
      displayname: string;
      createdAt: number;
      editedAt?: number;
    };
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class Note extends Component<NoteSignature> {
  @action
  openFullView() {
    if (this.args.isInTimeline) {
      alert("Would have opened note's own view");
    } else {
      console.log("Alread in note specific view, can't open it again");
    }
  }
}
