import { action } from '@ember/object';
import { map } from '@ember/object/computed';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

const re = /.+@\S+\.\S+/;

export interface UtilMailEntrySignature {
  // The arguments accepted by the component
  Args: {
    data: { mail: string; valid: boolean };
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class UtilMailEntry extends Component<UtilMailEntrySignature> {
  @tracked mailOk = this.args.data.valid;
  @action checkMail() {
    this.args.data.valid = re.test(this.args.data.mail);
    this.mailOk = this.args.data.valid;
  }
}
