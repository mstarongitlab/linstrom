import { action } from '@ember/object';
import Component from '@glimmer/component';

export interface UtilMultiselectSignature {
  // The arguments accepted by the component
  Args: {
    elements: Array<{ name: string; checked: boolean; description: string }>;
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class UtilMultiselect extends Component<UtilMultiselectSignature> {
  @action onChange() {
    console.log(this.args.elements);
  }
}
