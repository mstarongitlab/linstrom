import MutableArray from '@ember/array/mutable';
import { action } from '@ember/object';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

export interface UtilStringArraySignature {
  // The arguments accepted by the component
  Args: {
    list: MutableArray<{ value: string }>;
    prefix: string;
    onNewElement: (index: number) => void;
    onDeleteElement: (index: number) => void;
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class UtilStringArray extends Component<UtilStringArraySignature> {
  @action addElement() {
    MutableArray.apply(this.args.list);
    this.args.list.pushObject({ value: '' });
    if (this.args.onNewElement)
      this.args.onNewElement(this.args.list.length - 1);
  }

  @action removeElement(index: number) {
    MutableArray.apply(this.args.list);
    //let index = this.args.list.find((elem) => elem == content)
    //let index = this.listCopy.findIndex((d) => d == content)
    this.args.list.removeAt(index);
    if (this.args.onDeleteElement) this.args.onDeleteElement(index);
  }

  transformArrayIntoUsable(arr: Array<string>): { [key: number]: string } {
    const out: { [key: number]: string } = {};
    const tmp = arr.map((elem: string, index: number) => {
      out[index] = elem;
      return elem;
    });
    return out;
  }

  countElemsInObj(obj: any): number {
    let count = 0;

    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) ++count;
    }

    return count;
  }
}
