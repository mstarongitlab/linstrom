import { action } from '@ember/object';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

export interface NoteContentSignature {
  // The arguments accepted by the component
  Args: {
    content: string;
    preFormatted: boolean;
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class NoteContent extends Component<NoteContentSignature> {
  // Default to collapsed content
  @tracked visibleContent =
    this.args.content.length > 200 ? this.cutDownContent() : this.args.content;

  @tracked collapsed = true;
  @tracked canExpand = this.args.content.length > 200;

  @action
  expand() {
    this.visibleContent = this.args.content;
    this.collapsed = false;
  }

  @action
  collapse() {
    this.visibleContent =
      this.args.content.length > 200
        ? this.cutDownContent()
        : this.args.content;
    this.collapsed = true;
  }

  cutDownContent(): string {
    if (this.args.content.length > 200) {
      const words = this.args.content.split(' ');
      let outString = '';
      for (const word of words) {
        if (outString.length > 200) {
          return outString + '...';
        }
        outString += word + ' ';
      }
    } else {
      return this.args.content;
    }

    return '';
  }
}
