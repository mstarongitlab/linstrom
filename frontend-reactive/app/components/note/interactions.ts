import { action } from '@ember/object';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import isLandscape from 'frontend-reactive/helpers/isLandscape';

export interface NoteInteractionsSignature {
  // The arguments accepted by the component
  Args: {
    boostCount: number;
    totalLikeCount: number;
    reactions: {
      [key: string]: number;
    };
    hasBoosted: boolean;
    hasReacted: boolean;
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class NoteInteractions extends Component<NoteInteractionsSignature> {
  @tracked hasBoosted = this.args.hasBoosted;
  @tracked hasReacted = this.args.hasReacted;
  @tracked expandReactions = false;

  @action
  toggleBoost() {
    this.hasBoosted = !this.hasBoosted;
    console.log('boosted', this.hasBoosted);
  }

  @action
  toggleDefaultLike() {
    this.hasReacted = !this.hasReacted;
    console.log('reacted', this.hasReacted);
  }

  @action
  openCustomReactionSelector() {
    this.hasReacted = !this.hasReacted;
    console.log('sent custom reaction', this.hasReacted);
  }

  @action
  openAllReactions() {
    console.log('Toggle all reactions overview');
    this.expandReactions = !this.expandReactions;
  }
}
