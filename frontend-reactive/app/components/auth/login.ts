import { action } from '@ember/object';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

export interface AuthLoginSignature {
  // The arguments accepted by the component
  Args: {};
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class AuthLogin extends Component<AuthLoginSignature> {
  @tracked username = '';

  @action onLoginStart() {
    console.log('Starting login for username ' + this.username);
    // Check if username is approved for login
    // If it is, continue with login
  }
}
