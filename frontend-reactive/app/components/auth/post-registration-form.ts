import { action } from '@ember/object';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import isValidMail from 'frontend-reactive/helpers/is-valid-mail';

export interface AuthPostRegistrationFormSignature {
  // The arguments accepted by the component
  Args: {
    username: string;
  };
  // Any blocks yielded by the component
  Blocks: {
    default: [];
  };
  // The element to which `...attributes` is applied in the component template
  Element: null;
}

export default class AuthPostRegistrationForm extends Component<AuthPostRegistrationFormSignature> {
  @tracked displayname: string = this.args.username;
  @tracked description: string = '';
  @tracked gender: Array<{ value: string }> = [];
  @tracked beingTypes: Array<{
    name: string;
    checked: boolean;
    description: string;
  }> = [
    {
      name: 'Human',
      description: 'Human',
      checked: true,
    },
    {
      name: 'Cat',
      description: 'Cat',
      checked: false,
    },
    {
      name: 'Fox',
      description: 'Fox',
      checked: false,
    },
    {
      name: 'Dog',
      description: 'Dog',
      checked: false,
    },
    {
      name: 'Robot',
      description: 'Robot',
      checked: false,
    },
    {
      name: 'Doll',
      description: 'Doll',
      checked: false,
    },
  ];
  @tracked defaultpostmode: string = 'Public';
  @tracked followapproval: boolean = false;
  @tracked customProperties: Array<{ key: string; value: string }> = [];
  @tracked indexable: boolean = true;
  @tracked mail = { mail: '', valid: false };
  @tracked enableBlueskyIntegration = false;

  genderAddedHandler(newIndex: number) {
    console.log('gender added');
  }
  genderRemovedHandler(removedIndex: number) {
    console.log('gender removed');
  }

  @action test() {
    console.log(this.mail);
  }
}
