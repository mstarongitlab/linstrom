import EmberRouter from '@ember/routing/router';
import config from 'frontend-reactive/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('about');
  this.route('registerform');
  this.route('auth');
  this.route('testing');
});
