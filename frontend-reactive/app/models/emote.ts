import Model, { attr } from '@ember-data/model';
import type MediaMetadataModel from './media-metadata';
import type OriginServer from './origin-server';

export default class Emote extends Model {
  @attr declare metadataId: string;
  @attr declare metadata: MediaMetadataModel;
  @attr declare name: string;
  @attr declare serverId: number;
  @attr declare server: OriginServer;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    emote: Emote;
  }
}
