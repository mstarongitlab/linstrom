import Model, { attr } from '@ember-data/model';

export default class OriginServer extends Model {
  @attr() declare serverType: string;
  @attr() declare name: string;
  @attr() declare iconUrl: string;
  @attr() declare isSelf: boolean;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    originServer: OriginServer;
  }
}
