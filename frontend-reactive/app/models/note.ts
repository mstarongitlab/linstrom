import Model, { attr, hasMany, type AsyncHasMany } from '@ember-data/model';
import type AccountModel from './account';
import type MediaMetadataModel from './media-metadata';
import type OriginServer from './origin-server';

export default class Note extends Model {
  @attr declare content: string;
  @attr declare originServer: OriginServer;
  @attr declare originServerId: number;
  @attr declare reactionCount: number;
  @attr declare createdAt: Date;
  @attr declare updatedAt: Date;
  @attr declare author: AccountModel;
  @attr declare authorId: string;
  @attr declare contentWarning?: string;
  @attr declare inReplyToId?: string;
  @attr declare quotesId?: string;
  @attr declare emoteIds?: Array<string>;
  @hasMany('media-metadata')
  declare attachments: AsyncHasMany<MediaMetadataModel>;
  @attr declare attachmentIds: Array<string>;
  @attr declare accessLevel: number;
  @hasMany('account') declare pings?: AsyncHasMany<AccountModel>;
  @attr declare pingIds?: Array<string>;
  @attr declare reactionIds: Array<number>;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    note: Note;
  }
}
