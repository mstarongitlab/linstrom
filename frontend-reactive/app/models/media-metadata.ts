import Model, { attr } from '@ember-data/model';

export default class MediaMetadata extends Model {
  @attr declare createdAt: Date;
  @attr declare updatedAt: Date;
  @attr declare isRemote: boolean;
  @attr declare url: string;
  @attr declare mimeType: string;
  @attr declare name: string;
  @attr declare altText: string;
  @attr declare blurred: boolean;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    mediaMetadata: MediaMetadata;
  }
}
