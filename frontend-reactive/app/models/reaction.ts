import Model, { attr } from '@ember-data/model';
import type EmoteModel from './emote';

export default class Reaction extends Model {
  @attr declare noteId: string;
  @attr declare reactorId: string;
  @attr declare emoteId: number;
  @attr declare emote: EmoteModel;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    reaction: Reaction;
  }
}
