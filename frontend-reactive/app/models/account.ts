import Model, { attr, hasMany, type AsyncHasMany } from '@ember-data/model';
import type CustomAccountFieldModel from './custom-account-field';
import type MediaMetadataModel from './media-metadata';
import type OriginServer from './origin-server';

export default class Account extends Model {
  @attr declare createdAt: Date;
  @attr declare updatedAt: Date;
  @attr declare username: string;
  @attr declare originServer: OriginServer;
  @attr declare originServerId: number;
  @attr declare displayName: string;
  @hasMany('custom-account-field')
  declare customFields: AsyncHasMany<CustomAccountFieldModel>;
  @attr declare customFieldIds: Array<number>;
  @attr declare isBot: boolean;
  @attr declare description: string;
  @attr declare icon: MediaMetadataModel;
  @attr declare iconId: string;
  @attr declare banner: MediaMetadataModel;
  @attr declare bannerId: string;
  @attr declare background: MediaMetadataModel;
  @attr declare backgroundId: string;
  @attr declare relationIds: Array<number>;
  @attr declare indexable: boolean;
  @attr declare restrictedFollow: boolean;
  @attr declare identifiesAs: Array<string>;
  @attr declare pronouns: Array<string>;
  @attr declare roles: Array<string>;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    account: Account;
  }
}
