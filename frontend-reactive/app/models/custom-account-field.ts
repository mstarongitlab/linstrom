import Model, { attr } from '@ember-data/model';

export default class CustomAccountField extends Model {
  @attr declare createdAt: Date;
  @attr declare updatedAt: Date;
  @attr declare key: string;
  @attr declare value: string;
  @attr declare verified?: boolean;
  @attr declare belongsToId: string;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    customAccountField: CustomAccountField;
  }
}
