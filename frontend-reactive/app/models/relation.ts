import Model, { attr } from '@ember-data/model';

export default class Relation extends Model {
  @attr declare createdAt: Date;
  @attr declare updatedAt: Date;
  @attr declare fromId: string;
  @attr declare toId: string;
  @attr declare requested: boolean;
  @attr declare accepted: boolean;
}

declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    relation: Relation;
  }
}
