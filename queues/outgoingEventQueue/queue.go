// Queue for outbound events
// Actual queue here since other servers can't be expected to go at the same speed as Linstrom (be it slower or faster)
// This queue should enforce data consistency (new jobs are first stored in the db) and a fair processing speed for
// other servers, depending on their processing speed
package outgoingeventqueue
