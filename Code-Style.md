# Code style guide

## Go

- Use `gofmt`
- `jsonapi` fields must use dasharised names
  (eg `some-field` instead of `some_field` or `someField`)
- Function, struct and variable names must describe their purpose adequatly.
  Making them longer for that purpose is fine
- Follow [https://go.dev/doc/effective_go]
  (which also happens to be convenient place to start learning Go)
- Always use zerolog for console output. In http handlers,
  use `hlog.FromRequest` to get a logger instance prepared with a bunch of metadata
- As Linstrom is both intended for active use as well as providing a learning resource,
  all functions and structs must be documented

## JS/TS

<!-- TODO: Fill this out -->

- Biome should do most of the work
